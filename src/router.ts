import { Application } from "express";
import HomeController from "./controllers/HomeController";
import ArticleController from "./controllers/ArticleController";

export default function route(app: Application) {
    /** Static pages **/
    app.get('/', (req, res) => {
        HomeController.index(req, res);
    });

    app.get('/projets', (req, res) => {
        HomeController.projets(req, res);
    });

    app.get('/blog', (req, res) => {
        HomeController.blog(req, res)
    });

    app.get('/contact', (req, res) => {
        HomeController.contact(req, res)
    });

    app.get('/admin', (req, res) => {
        HomeController.admin(req, res)
    });
    app.post('/article-create', (req, res) => {
        ArticleController.create(req, res)
    });

    app.get('/article-read/:id', (req, res) => {
        ArticleController.read(req, res)
    });

    app.get('/article-update/:id', (req, res) => {
        ArticleController.showFormUpdate(req, res)
    });

    app.get('/article-update/:id', (req, res) => {
        ArticleController.showFormUpdate(req, res)
    });

    app.get('/article-delete/:id', (req, res) => {
        ArticleController.delete(req, res)
    });
}
