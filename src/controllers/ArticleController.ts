import { Request, Response } from "express-serve-static-core";

export default class ArticleController {

    static index(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const articles = db.prepare("SELECT * FROM articles").all();
        //console.log(blog);


        res.render('pages/article', {
            title: 'Articles',
            articles: articles
        });
    }

    static showForm(req: Request, res: Response): void {
        res.render('pages/article-create');
    }

    static create(req: Request, res: Response): void {
        const db = req.app.locals.db;
        db.prepare('INSERT INTO articles ("title", "content") VALUES (?,?)').run(req.body.title, req.body.content);

        ArticleController.index(req, res);
    }

    static read(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const article = db.prepare('SELECT * FROM articles WHERE id=?').get(req.params.id);

        res.render('pages/article', {
            article: article
        });
    }
    static showFormUpdate(req: Request, res: Response) {
        const db = req.app.locals.db;

        const article = db.prepare('SELECT * FROM articles WHERE id = ?').get(req.params.id);
        res.render('pages/article-update', {
            article: article
        });
    }

    static update(req: Request, res: Response) {
        const db = req.app.locals.db;
        db.prepare('UPDATE articles SET title = ?, content = ? WHERE id = ?').run(req.body.title, req.body.content, req.params.id);

        ArticleController.index(req, res);
    }

    static delete(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM articles WHERE id=?').run(req.params.id);

        ArticleController.index(req, res);
    }
}