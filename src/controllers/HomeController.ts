import { Request, Response } from "express-serve-static-core";

export default class HomeController
{
    static index(req: Request, res: Response): void
    {
        res.render('pages/index', {
            title: 'Accueil',
        });
    }

    static blog(req: Request, res: Response): void
    {
        res.render('pages/blog', {
            title: 'Blog',
        });
    }

    static projets (req: Request, res: Response): void
    {
        res.render('pages/projets', {
            title: 'Projets',
        });
    }

    static contact(req: Request, res: Response): void
    {
        res.render('pages/contact', {
            title: 'Contact',
        });
    }

    static admin(req: Request, res: Response): void
    {
        res.render('pages/admin', {
            title: 'Admin',
        });
    }



}