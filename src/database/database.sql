-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Alexis
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-11 16:47
-- Created:       2021-12-11 16:27
PRAGMA foreign_keys = OFF;

-- Schema: mydb
--ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "articles"(
  "id_articles" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "title" LONGTEXT NOT NULL,
  "content" LONGTEXT NOT NULL,
  "author" VARCHAR(45) NOT NULL,
  "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "modified_at" DATETIME,
  "deleted_at" DATETIME
);
CREATE TABLE "users"(
  "id_user" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "username" VARCHAR(45),
  "password" VARCHAR(45),
  "email" LONGTEXT,
  "created_at" DATETIME,
  "modified_at" DATETIME,
  "deleted_at" DATETIME
);
CREATE TABLE "users_has_articles"(
  "id_pivot" INTEGER NOT NULL,
  "users_id_user" INTEGER NOT NULL,
  "articles_id_articles" INTEGER NOT NULL,
  PRIMARY KEY("id_pivot","users_id_user","articles_id_articles"),
  CONSTRAINT "fk_users_has_articles_users"
    FOREIGN KEY("users_id_user")
    REFERENCES "users"("id_user"),
  CONSTRAINT "fk_users_has_articles_articles1"
    FOREIGN KEY("articles_id_articles")
    REFERENCES "articles"("id_articles")
);
CREATE INDEX "users_has_articles.fk_users_has_articles_articles1_idx" ON "users_has_articles" ("articles_id_articles");
CREATE INDEX "users_has_articles.fk_users_has_articles_users_idx" ON "users_has_articles" ("users_id_user");
COMMIT;
